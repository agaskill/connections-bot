let myinfo = null
let allusers = null

function getMyInfo(env) {
    if (!myinfo) {
        myinfo = invokeSlackApi(env, "auth.test", {})
    }
    return myinfo
}

function getAllUsers(env) {
    if (!allusers) {
        allusers = invokeSlackApi(env, "users.list", {})
    }
    return allusers
}

const event_handlers = {
    async message(body, env) {
        const { team_id } = body
        const { channel, user, ts, thread_ts } = body.event
        console.log(body.event.text)
        const me = await getMyInfo(env)
        if (me.user_id === user) {
            console.log("message from self, ignoring")
            return
        }
        const puzzle = parse_puzzle(body.event.text)
        if (puzzle) {
            console.log(`puzzle ${puzzle.number} detected from user ${user} in channel ${channel}.  Score is ${puzzle.score}`)
            await update_scores(team_id, channel, user, ts, puzzle, env)
            await send_reply(channel, thread_ts || ts, puzzle, env)
        }
        else {
            console.log(`message ${ts} is not a puzzle`)
        }
        if (body.event.text.indexOf("<@" + me.user_id + ">") >= 0) {
            // bot mention
            if (/leaderboard/.test(body.event.text)) {
                const forchannel = /for channel <#([^|]+)/.exec(body.event.text)
                if (forchannel) {
                    await post_leaderboard(team_id, channel, forchannel[1], env)
                } else {
                    await post_leaderboard(team_id, channel, channel, env)
                }
            }
            else if (/reprocess this channel/.test(body.event.text)) {
                const reprocess_result = await reprocess_messages(channel, env)
                await send_reprocess_result(env, channel, reprocess_result)
            } else {
                await respond_with_chatgpt(channel, thread_ts || ts, env)
            }
        }
    }
}

const handlers = {
    url_verification(body) {
        return new Response(body.challenge, {
            headers: {
                'content-type': 'text/plain'
            }
        })
    },
    async reprocess_channel(body, env) {
        let res = await reprocess_messages(body.channel, env)
        return new Response(`Found ${res.puzzles_found} puzzles and updated ${res.scores_updated} scores in ${res.messages_read} messages`)
    },
    async event_callback(body, env, ctx) {
        if (env.slack_verification_token !== body.token) {
            return new Response("invalid or missing verification token", { status: 403 })
        }
        const handler = event_handlers[body.event.type]
        if (handler) {
            ctx.waitUntil(handler(body, env))
        } else {
            console.log("received unknown event " + body.event.type)
        }
        return new Response(null, { status: 202 })
    }
}

export default {
    async fetch(request, env, ctx) {
        const body = await request.json()
        const handler = handlers[body.type]
        if (handler) {
            return await handler(body, env, ctx)
        }
        console.log("unhandled type " + body.type)
        return new Response(null, { status: 202 })
    }
};

const sqlFindExisting = "SELECT ts FROM scores WHERE team_id = ? AND channel = ? AND user = ? AND puzzle = ?"
const sqlInsertScore = "INSERT INTO scores (team_id, channel, user, puzzle, score, ts) VALUES (?, ?, ?, ?, ?, ?)"
const sqlSummaryScores = "SELECT user, count(score) n, avg(score) avg_score FROM scores WHERE team_id = ? AND channel = ? GROUP BY user ORDER BY avg_score DESC"

async function post_leaderboard(team_id, channel, forChannel, env) {
    const data = await env.database
        .prepare(sqlSummaryScores)
        .bind(team_id, forChannel)
        .all()
    const users = await getAllUsers(env)
    for (const r of data.results) {
        const user = users.members.find(u => u.id === r.user)
        if (user) {
            r.name = user.display_name || user.real_name || user.name
        }
    }
    const text = data.results
        .map(d => `${d.name || d.user}: average score ${Math.round(d.avg_score)} over ${d.n} puzzles`)
        .join('\n')
    await slackPostMessage(env, {
        channel,
        text,
    })
}

function send_reprocess_result(env, channel, res) {
    return slackPostMessage(env, {
        channel,
        text: `Found ${res.puzzles_found} puzzles and updated ${res.scores_updated} scores in ${res.messages_read} messages`
    })
}

function slackPostMessage(env, msg) {
    return invokeSlackApi(env, "chat.postMessage", msg)
}

async function invokeSlackApi(env, method, msg) {
    const res = await fetch("https://slack.com/api/" + method, {
        method: "POST",
        headers: {
            'content-type': typeof msg === 'string' ? 
                'application/x-www-form-urlencoded' : 
                'application/json;charset=utf-8',
            'authorization': 'Bearer ' + env.slack_api_token,
        },
        body: typeof msg === 'string' ? msg : JSON.stringify(msg)
    })
    if (res.ok) {
        const body = await res.json()
        if (body.ok) {
            return body
        }
        console.log(`invokeSlackApi error: ${body.error}`)
    } else {
        console.log(`invokeSlackApi error ${res.status} ${res.statusText}`)
    }
}

async function reprocess_messages(channel, env) {
    console.log("Reprocessing all messages in channel " + channel)
    const findExisting = env.database.prepare(sqlFindExisting)
    const insertScore = env.database.prepare(sqlInsertScore)
    let messages_read = 0
    let puzzles_found = 0
    let scores_updated = 0
    let cursor = undefined
    do {
        const res = await invokeSlackApi(env, "conversations.history", {
            channel,
            cursor,
        })
        if (res.messages) {
            for (const msg of res.messages) {
                messages_read++
                const process_result = await process_message(msg, channel, findExisting, insertScore)
                if (process_result > 0) {
                    puzzles_found++
                }
                if (process_result > 1) {
                    scores_updated++
                }
            }
        }
        if (res.has_more) {
            cursor = res.response_metadata.next_cursor
        } else {
            cursor = undefined
        }
    } while (cursor)
    return { messages_read, puzzles_found, scores_updated }
}

async function process_message(msg, channel, findExisting, insertScore) {
    let result
    const puzzle = parse_puzzle(msg.text)
    if (puzzle) {
        const existing = await findExisting.bind(msg.team, channel, msg.user, puzzle.number).first('ts')
        if (existing) {
            result = 1
        } else {
            await insertScore.bind(msg.team, channel, msg.user, puzzle.number, puzzle.score, msg.ts).run()
            result = 2
        }
    } else {
        result = 0
    }
    return result
}

async function update_scores(team_id, channel, user, ts, puzzle, env) {
    const existing = await env.database
        .prepare(sqlFindExisting)
        .bind(team_id, channel, user, puzzle.number)
        .first('ts')
    if (existing) {
        console.log(`An existing post for this puzzle was already seen at ts ${existing}`)
    }
    else {
        console.log(`Updating score for user ${user}, channel ${channel}, workspace ${team_id}, message ${ts}, puzzle ${puzzle.number}, score ${puzzle.score}`)
        await env.database
            .prepare(sqlInsertScore)
            .bind(team_id, channel, user, puzzle.number, puzzle.score, ts)
            .run()
    }
}

function send_reply(channel, thread_ts, puzzle, env) {
    console.log(`Sending to channel ${channel}, thread ${thread_ts}, score ${puzzle.score}`)
    return slackPostMessage(env, {
        channel,
        thread_ts,
        text: `Score: ${puzzle.score}`
    })
}

function parse_puzzle(text) {
    const match = /Connections\s+Puzzle #(\d+)((?:\s+(?::large_(?:yellow|green|blue|purple)_square:){4})+)/.exec(text)
    if (match) {
        const number = Number(match[1])
        const score = getScore(match[2])
        if (isFinite(score)) {
            return { number, score }
        }
    }
    return undefined
}

function getScore(text) {
    const attempts = normalizeGuesses(text);
    if (!attempts) {
        return NaN
    }
    let score = 0;
    let solvedCount = 0
    attempts.forEach((guesses, round) => {
        const val = roundValue(guesses, round);
        if (val > 0 && ++solvedCount < 4) {
            score += val;
        }
    });
    return score;
}

const values = {
    yellow: 1,
    green: 2,
    blue: 3,
    purple: 4,
}

function normalizeGuesses(text) {
    const rows = []
    let row = []
    for (const m of text.matchAll(/:large_(yellow|green|blue|purple)_square:/g)) {
        const color = m[1]
        if (row.length == 4) {
            rows.push(row)
            row = []
        }
        row.push(values[color])
    }
    if (row.length == 4) {
        rows.push(row)
    } else {
        // if the last row is incomplete, it's not a valid puzzle
        return null
    }
    if (rows.length < 4) {
        // if there are fewer than four rows, it's not a valid puzzle
        return null
    }
    return rows
}

function roundValue(guesses, round) {
    const solvedValue = solved(guesses)
    if (isFinite(solvedValue)) {
        return (4 + solvedValue * 2) * (7 - round)
    }
    return 0
}

function solved(guesses) {
    const first = guesses[0]
    if (guesses.every(g => g === first)) {
        return first - 1
    }
    return NaN
}

async function respond_with_chatgpt(channel, thread_ts, env) {
    const messages = [{
        role: "system",
        content: "You are a bot that scores puzzle solutions from 0 to 150"
    }]
    const me = await getMyInfo()
    console.log(`Getting all messages in thread ${thread_ts} of channel ${channel} (I am ${me.user_id})`)
    const replies = await invokeSlackApi(env, "conversations.replies",
        "channel=" + encodeURIComponent(channel) +
        "&ts=" + encodeURIComponent(thread_ts))
    for (const msg of replies.messages) {
        messages.push({
            role: me.user_id === msg.user ? 'assistant' : 'user',
            content: msg.text
        })
    }
    console.log(`Feeding ${messages.length} messages to ChatGPT`)
    const chat = await invokeChatGpt(env, messages)
    const text = chat.choices[0].message.content
    await slackPostMessage(env, { channel, thread_ts, text })
}

async function invokeChatGpt(env, messages) {
    const res = await fetch("https://api.openai.com/v1/chat/completions", {
        method: "POST",
        headers: {
            "Authorization": "Bearer " + env.chatgpt_token,
            "OpenAI-Organization": env.chatgpt_organization,
            "Content-Type": "application/json"
        },
        body: JSON.stringify({
            model: "gpt-3.5-turbo",
            messages: messages,
        })
    })
    if (res.ok) {
        const json = await res.json()
        return json
    } else {
        console.log(`Error invoking chatGpt: ${res.status} ${res.statusText}`)
        return {}
    }
}